using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Filter;
using NUnit.Framework;

namespace Newsdeck.Data.EF.UnitTest.Test.Blob
{
    public class BlobTest
    {
        

        private async Task<List<IBlobEntity>> GetAll()
        {
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();
            var readSession = sessionProvider.OpenReadSession();
            var blobReadRepository = readSession.GetBlobReadRepository();
            var blobQuery = blobReadRepository.Get();
            blobQuery.AddFilter(new DifferentFilter<IBlobEntity>(entity => entity.Identifier, 0l));
            blobQuery.AddFilter(new GreaterOrEqualThenFilter<IBlobEntity>(entity => entity.LastModificationUnixTime, 500l));
            return await blobQuery.AsListAsync(cancellationToken);
        }

        [Test]
        public async Task TestBlob()
        {
            const int msWaiting = 3000;
            
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();

            IBlobEntity createdEntity;
            IBlobEntity editedEntity;

            await using (var readSession = sessionProvider.OpenWriteSession())
            {
                var blobReadRepository = readSession.GetBlobWriteRepository();
                var m = new CreateBlobModel
                {
                    Data =
                    {
                        { "test", 0 }
                    },
                    ContentType = "text/json"
                };
                var id = await blobReadRepository.CreateAsync(m, cancellationToken);
                createdEntity = await blobReadRepository.GetWithIdAsync(id, cancellationToken);
                await readSession.CommitAsync(cancellationToken);
            }

            Thread.Sleep(msWaiting);
            
            await using (var readSession2 = sessionProvider.OpenWriteSession())
            {
                var blobReadRepository2 = readSession2.GetBlobWriteRepository();
                var edit = new EditBlobModel(createdEntity)
                {
                    
                };
                edit.Data["testNew"] = "test2Valid";
                await blobReadRepository2.EditAsync(createdEntity.Identifier, edit, cancellationToken);
                editedEntity = await blobReadRepository2.GetWithIdAsync(createdEntity.Identifier, cancellationToken);
                await readSession2.CommitAsync(cancellationToken);
            }

            var all = await GetAll();
            
            Assert.GreaterOrEqual(editedEntity.LastModificationUnixTime, editedEntity.CreationUnixTime + msWaiting);
            Assert.GreaterOrEqual(editedEntity.LastModificationUnixTime, createdEntity.LastModificationUnixTime + msWaiting);
        }
    }
}