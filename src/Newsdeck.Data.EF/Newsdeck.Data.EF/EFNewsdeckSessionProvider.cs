using Microsoft.Extensions.Logging;

namespace Newsdeck.Data.EF
{
    internal class EFNewsdeckSessionProvider : INewsdeckSessionProvider
    {
        private readonly EFNewsdeckConfiguration _configuration;
        private readonly ILoggerFactory _loggerFactory;

        public EFNewsdeckSessionProvider(EFNewsdeckConfiguration configuration
            , ILoggerFactory loggerFactory)
        {
            _configuration = configuration;
            _loggerFactory = loggerFactory;
        }

        public IReadSession OpenReadSession()
        {
            return new EFReadSession(new NewsdeckDbContext(_configuration, _loggerFactory));
        }

        public IWriteSession OpenWriteSession()
        {
            return new EFWriteSession(new NewsdeckDbContext(_configuration, _loggerFactory));
        }
    }
}