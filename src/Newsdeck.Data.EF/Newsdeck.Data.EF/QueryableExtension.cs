using System;
using System.Linq;
using System.Linq.Expressions;
using Newsdeck.Data.Filter;
using Newsdeck.Data.Order;

namespace Newsdeck.Data.EF
{
    internal static class QueryableExtension
    {

        public static IQueryable<TEntity> QWhere<TContract, TEntity>(this IQueryable<TEntity> source,
            PropertyFilter<TContract> f,
            Func<MemberExpression, BinaryExpression> binaryExpression) where TEntity : TContract
        {
            // The input type
            var propertyParameter = Expression.Parameter(typeof(TEntity));
            var fs = Expression.Property(propertyParameter, f.PropertyInfo);
            var r = binaryExpression.Invoke(fs);
            // The lambda expression for use with Linq
            Expression<Func<TEntity, bool>> finalExpression 
                = Expression.Lambda<Func<TEntity, bool>>(r, propertyParameter);
            return source.Where(finalExpression);
        }
        public static IQueryable<TEntity> QOrder<TContract, TEntity>(this IQueryable<TEntity> source,
            AOrder<TContract> f,
            bool asc) where TEntity : TContract
        {
            // The lambda expression for use with Linq
            Expression<Func<TEntity, object>> finalExpression 
                = CreateProperty<TContract, TEntity, object>(f);
            
            return asc 
                ? source.OrderBy(finalExpression) 
                : source.OrderByDescending(finalExpression);
        }
        
        
        private static Expression<Func<TEntity, TProperty>> CreateProperty<TContract, TEntity, TProperty>(
            AOrder<TContract> property)
        where TEntity : TContract
        {
            ParameterExpression ep = Expression.Parameter(typeof(TEntity), "x");
            MemberExpression em = Expression.Property(ep, property.PropertyInfo);
            var el = Expression.Lambda<Func<TEntity, TProperty>>(Expression.Convert(em, typeof(object)), ep);
            return el;
        }
    }
}