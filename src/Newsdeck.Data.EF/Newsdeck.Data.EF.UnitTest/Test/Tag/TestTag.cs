using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Filter;
using NUnit.Framework;

namespace Newsdeck.Data.EF.UnitTest.Test.Tag
{
    public class TestTag
    {
        [Test]
        public async Task TestGetTag()
        {
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();
            
            await using (var session = sessionProvider.OpenReadSession())
            {
                var repo = session.GetTagReadRepository();
                var tagsPool2 = await repo
                    .Get()
                    .FilterBy(new InFilter<ITagEntity>(
                        o => o.Identifier, 
                        new List<object> { 1,2,3,4 }
                    ))
                    .AsListAsync(cancellationToken);
                Assert.AreEqual(4, tagsPool2.Count);
            }
            
        }
    }
}