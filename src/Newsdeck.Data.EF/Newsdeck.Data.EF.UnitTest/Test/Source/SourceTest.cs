using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Filter;
using NUnit.Framework;

namespace Newsdeck.Data.EF.UnitTest.Test.Source
{
    public class SourceTest
    {
        [Test]
        public async Task TestRetrieveSources()
        {
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();

            List<ISourceEntity>? results = null;
            await using (var session = sessionProvider.OpenReadSession())
            {
                var repository = session.GetSourceReadRepository();

                results = await repository.Get()
                    .OrderDesc(o => o.CreationUnixTime)
                    .FilterBy(new DifferentFilter<ISourceEntity>(o => o.Type, "differentTest"))
                    .Limit(500)
                    .AsListAsync(cancellationToken);
            }

            Assert.NotNull(results);
        }

        [Theory]
        [TestCase("https://dev.to/feed", 1)]
        [TestCase("   https://dev.to/feed   ", 1)]
        [TestCase("notanexistingfeed", null)]
        public async Task TestFindMatchingSource(string url, long? sourceId)
        {
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();
            
            await using (var session = sessionProvider.OpenReadSession())
            {
                var repository = session.GetSourceReadRepository();
                var results = await repository.FindMatchingSource(url, cancellationToken);
                Assert.AreEqual(sourceId, results?.Identifier);
            }
        }

        [Test]
        public async Task TestCreateAndEditAndDelete()
        {
            var model = new TestSourceModel
            {
                Type = "Test",
                JsonData = "{}",
                SourceIconIdentifier = null,
                LastTimeChangedUnixTime = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                NextCheckAfterUnixTime = DateTimeOffset.Now.ToUnixTimeMilliseconds()
            };
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();

            long? id;
            await using (var session = sessionProvider.OpenWriteSession())
            {
                var repo = session.GetSourceWriteRepository();

                id = await repo.CreateAsync(model, cancellationToken);
                model.JsonData = "{'edited':4}";
                await repo.EditAsync(id.Value, model, cancellationToken);
                await session.CommitAsync(cancellationToken);
            }

            // retrieve edited in ram
            ISourceEntity? entity;
            await using (var session = sessionProvider.OpenReadSession())
            {
                var repo = session.GetSourceReadRepository();
                if (!id.HasValue)
                {
                    throw new Exception("No value for id");
                }

                entity = await repo.GetWithIdAsync(id.Value, cancellationToken);
            }

            // delete
            await using (var session = sessionProvider.OpenWriteSession())
            {
                var repo = session.GetSourceWriteRepository();
                if (!id.HasValue)
                {
                    throw new Exception("No value for id");
                }

                await repo.DeleteAsync(id.Value, cancellationToken);
                await session.CommitAsync(cancellationToken);
            }

            Assert.AreEqual(model.JsonData, entity.JsonData);
        }
    }
}