using System.Collections.Generic;
using Newsdeck.Data.Data;
using Newtonsoft.Json;

namespace Newsdeck.Data.EF.UnitTest.Test.Publication
{
    public abstract class APublicationModel : AEntityDataModel, IPublicationData
    {
        public Dictionary<string, object> Data { get; protected set; } = new Dictionary<string, object>();

        public string JsonData => JsonConvert.SerializeObject(Data);
        public long SourceIdentifier { get; set; }
        public long? PublicationCoverIdentifier { get; set; }
        public string OriginalGuid { get; set; } = string.Empty;
        public long PublicationDateUnixTime { get; set; }
    }
}