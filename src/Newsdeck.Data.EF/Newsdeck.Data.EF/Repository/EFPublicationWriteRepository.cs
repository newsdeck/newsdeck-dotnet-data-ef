using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newsdeck.Data.Data;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Model;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF.Repository
{
    internal class EFPublicationWriteRepository : EFPublicationReadRepository, IPublicationWriteRepository
    {

        private readonly EFWriteSession _efWriteSession;
        
        public EFPublicationWriteRepository(EFWriteSession efWriteSession)
            : base(efWriteSession)
        {
            _efWriteSession = efWriteSession;
        }

        public new IWriteSession Session => _efWriteSession;
        
        public async Task<long> CreateAsync(ICreatePublicationModel model, CancellationToken cancellationToken)
        {
            var set = GetDebSet();
            var res = await set.AddAsync(ToEntity(model), cancellationToken);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
            return res.Entity.Identifier;
        }

        public async Task EditAsync(long identifier, IEditPublicationModel model, CancellationToken cancellationToken)
        {
            var item = await _GetWithIdAsync(identifier, cancellationToken);
            Apply(item, model);
            GetDebSet().Update(item);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(long identifier, CancellationToken cancellationToken)
        {
            var item = await _GetWithIdAsync(identifier, cancellationToken);
            GetDebSet().Remove(item);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(IEnumerable<long> identifier, long sourceIdentifier, CancellationToken cancellationToken)
        {
            var ids = identifier.ToList();
            var set = GetDebSet();
            var items = await set.Where(o => ids.Contains(o.Identifier))
                .Where(o => o.SourceIdentifier == sourceIdentifier)
                .ToListAsync(cancellationToken);
            set.RemoveRange(items);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task LinkPublicationToTagAsync(long identifier, int tagIdentifier, CancellationToken cancellationToken)
        {
            var e = await _efWriteSession.Context.Tags.FindAsync(tagIdentifier);
            await _efWriteSession.Context.PublicationsTags.AddAsync(new EFPublicationTagEntity
            {
                PublicationIdentifier = identifier,
                TagIdentifier = e.Identifier
            }, cancellationToken);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task RemoveLinkPublicationToTagAsync(long identifier, int tagIdentifier, CancellationToken cancellationToken)
        {
            var toDelete = await _efWriteSession.Context.PublicationsTags.Where(
                o => o.TagIdentifier.Equals(tagIdentifier)
                && o.PublicationIdentifier.Equals(identifier)).ToListAsync(cancellationToken); 
            _efWriteSession.Context.PublicationsTags.RemoveRange(toDelete);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task RemoveLinksPublicationToTagAsync(long identifier, IEnumerable<int> tagIdentifiers, CancellationToken cancellationToken)
        {
            var toDelete = await _efWriteSession.Context.PublicationsTags.Where(
                o => tagIdentifiers.Contains(o.TagIdentifier)
                     && o.PublicationIdentifier.Equals(identifier)).ToListAsync(cancellationToken); 
            _efWriteSession.Context.PublicationsTags.RemoveRange(toDelete);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task LinkPublicationToTagsAsync(long identifier, IEnumerable<int> tagIdentifiers, CancellationToken cancellationToken)
        {
            foreach (var tagIdentifier in tagIdentifiers)
            {
                var e = await _efWriteSession.Context.Tags.FindAsync(tagIdentifier);
                await _efWriteSession.Context.PublicationsTags.AddAsync(new EFPublicationTagEntity
                {
                    PublicationIdentifier = identifier,
                    TagIdentifier = e.Identifier
                }, cancellationToken);
            }
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        private EFPublicationEntity ToEntity(ICreatePublicationModel model)
        {
            var b = model.ToEntity<EFPublicationEntity>();
            _ApplyData(b, model);
            return b;
        }

        private void Apply(EFPublicationEntity entity, IEditPublicationModel model)
        {
            model.ApplyTo(entity);
            _ApplyData(entity, model);
        }

        private void _ApplyData(EFPublicationEntity entity, IPublicationData data)
        {
            entity.JsonData = data.JsonData;
            entity.SourceIdentifier = data.SourceIdentifier;
            entity.PublicationCoverIdentifier = data.PublicationCoverIdentifier;
            entity.OriginalGuid = data.OriginalGuid;
            entity.PublicationDateUnixTime = data.PublicationDateUnixTime;
        }
    }
}