using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Newsdeck.Data.EF.UnitTest.Test.DbContext
{
    public class TestDbContext
    {
        [Test]
        public async Task TestCreateDatabaseFromCode()
        {
            var instance = Instance.Get("./test-from-code.sqlite");
            var provider = instance.GetInstance<INewsdeckSessionProvider>();

            await using (var session = provider.OpenReadSession())
            {
                var repo = session.GetPublicationReadRepository();
                var all = await repo.Get().AsListAsync(CancellationToken.None);
                Assert.AreEqual(0, all.Count);
            }
        }
    }
}