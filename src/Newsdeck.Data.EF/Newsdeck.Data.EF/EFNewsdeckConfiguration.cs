using System;
using Microsoft.EntityFrameworkCore;

namespace Newsdeck.Data.EF
{
    internal class EFNewsdeckConfiguration : IEFNewsdeckConfiguration
    {
        public Action<DbContextOptionsBuilder> DbContextOptionBuilder { get; set; }
        
        public Action<DbContext> OnContextInitialization { get; set; }
    }
}