using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;
using NUnit.Framework;

namespace Newsdeck.Data.EF.UnitTest.Test.Publication
{
    public class PublicationTest
    {
        [Test]
        public async Task TestCreateAndEditPublication()
        {
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();

            await using (var session = sessionProvider.OpenWriteSession())
            {
                var blobQuery = session.GetBlobReadRepository().Get();
                blobQuery.SetLimit(1);
                var blobs = await blobQuery.AsListAsync(cancellationToken);
                var blob = blobs.First();
                
                var publicationQuery = session.GetSourceWriteRepository().Get();
                publicationQuery.SetLimit(1);
                var sources = await publicationQuery.AsListAsync(cancellationToken);
                var source = sources.First();
                
                var m = new CreatePublicationModel
                {
                    Data =
                    {
                        {"test", 1}
                    },
                    SourceIdentifier = source.Identifier,
                    PublicationCoverIdentifier = blob.Identifier,
                    OriginalGuid = $"GUID_TEST_PUB_{DateTimeOffset.Now.ToString()}"
                };
                var provider = session.GetPublicationWriteRepository();
                var createdId = await provider.CreateAsync(m, cancellationToken);
                var created = await provider.GetWithIdAsync(createdId, cancellationToken);

                var edit = new EditPublicationModel(created);
                edit.Data["publicationData"] = "Lorem ipsum";

                await provider.EditAsync(createdId, edit, cancellationToken);
                var edited = await provider.GetWithIdAsync(createdId, cancellationToken);
                
                await session.CommitAsync(cancellationToken);

                Assert.AreEqual(created.CreationUnixTime, edited.CreationUnixTime);
                Assert.AreEqual(created.JsonData, "{\"test\":1,\"publicationData\":\"Lorem ipsum\"}");
            }
        }
        
        [Test]
        public async Task TestPublication()
        {
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();


            await using (var readSession = sessionProvider.OpenReadSession())
            {
                var repo = readSession.GetPublicationReadRepository();
                var blobRepo = readSession.GetBlobReadRepository();
                var query = repo.Get();
                query.SetLimit(50);
                var publications = await query.AsListAsync(cancellationToken);
                foreach (var publicationEntity in publications)
                {
                    if (!publicationEntity.PublicationCoverIdentifier.HasValue)
                    {
                        throw new Exception("Publication.PublicationCoverIdentifier is null");
                    }
                    var cover = await blobRepo.GetWithIdAsync(publicationEntity.PublicationCoverIdentifier.Value, cancellationToken);
                    var o1 = publicationEntity.PublicationCover?.LastModificationUnixTime;
                    var o2 = cover?.LastModificationUnixTime;
                    DateTimeOffset? d1 = o1.HasValue ? DateTimeOffset.FromUnixTimeMilliseconds(o1.Value) : null;
                    DateTimeOffset? d2 = o2.HasValue ? DateTimeOffset.FromUnixTimeMilliseconds(o2.Value) : null;
                    Assert.True(d1 == d2);

                    var tags = publicationEntity.Tags;
                    var source = publicationEntity.Source;
                }
            }
        }

        [Test]
        public async Task TestAddAndRemoveTags()
        {
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();
            List<ITagEntity> tagsPool;
            const long publicationId = 7;

            await using (var session = sessionProvider.OpenReadSession())
            {
                var repo = session.GetTagReadRepository();
                tagsPool = await repo.Get().AsListAsync(cancellationToken);
            }

            await using (var session = sessionProvider.OpenWriteSession())
            {
                var repo = session.GetPublicationWriteRepository();

                var entityToTest = await repo.GetWithIdAsync(publicationId, cancellationToken);
                
                await repo.LinkPublicationToTagsAsync(entityToTest.Identifier,
                    tagsPool.Select(o => o.Identifier),
                    cancellationToken);

                entityToTest = await repo.GetWithIdAsync(publicationId, cancellationToken);
                Assert.AreEqual(tagsPool.Count, entityToTest.Tags.Count());
                
                await repo.RemoveLinksPublicationToTagAsync(entityToTest.Identifier,
                    tagsPool.Select(o => o.Identifier),
                    cancellationToken);
                
                entityToTest = await repo.GetWithIdAsync(publicationId, cancellationToken);
                Assert.AreEqual(0, entityToTest.Tags.Count());

                await session.CommitAsync(cancellationToken);
            }
            
            // Assert we still have the same number of tags
            await using (var session = sessionProvider.OpenReadSession())
            {
                var repo = session.GetTagReadRepository();
                var tagsPool2 = await repo.Get().AsListAsync(cancellationToken);
                Assert.AreEqual(tagsPool.Count, tagsPool2.Count);
            }
        }
    }
}