using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using Newsdeck.Data.Entity;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace Newsdeck.Data.EF.Entity
{
    [Table("blob")]
    internal class EFBlobEntity : EFBaseEntity, IBlobEntity
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Identifier { get; set; }

        [Column("originalUrl")]
        public string OriginalUrl { get; set; }
        
        [Column("contentType")]
        public string ContentType { get; set; }

        [Column("jsonData")]
        public string JsonData { get; set; }

        [Column("handlerJsonData")]
        public string HandlerJsonData { get; set; }
    }
}