using Microsoft.EntityFrameworkCore;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF.Repository
{
    internal class EFBlobReadRepository : EFBaseReadRepository<EFBlobEntity, IBlobEntity, long>, IBlobReadRepository
    {

        public EFBlobReadRepository(EFReadSession efReadSession) 
            : base(efReadSession)
        {
        }

        protected override DbSet<EFBlobEntity> GetDebSet()
        {
            return EFReadSession.Context.Blobs;
        }

        public IDataResult<IBlobEntity, long> Get()
        {
            return new EntityDataResult<IBlobEntity, EFBlobEntity, long>(GetDebSet().AsQueryable());
        }
    }
}