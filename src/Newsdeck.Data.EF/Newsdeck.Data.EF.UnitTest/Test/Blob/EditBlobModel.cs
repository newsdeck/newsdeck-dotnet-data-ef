using Newsdeck.Data.Entity;
using Newsdeck.Data.Model;

namespace Newsdeck.Data.EF.UnitTest.Test.Blob
{
    public class EditBlobModel : ABlobDataModel, IEditBlobModel
    {
        public EditBlobModel(IBlobEntity entity)
        {
            Data = MakeDictionary(entity.JsonData);
            ContentType = entity.ContentType;
            OriginalUrl = entity.OriginalUrl;
            HandlerData = MakeDictionary(entity.HandlerJsonData);
        }
    }
}