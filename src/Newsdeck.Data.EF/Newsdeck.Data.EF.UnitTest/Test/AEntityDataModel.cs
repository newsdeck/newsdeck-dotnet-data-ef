using System.Collections.Generic;
using Newtonsoft.Json;

namespace Newsdeck.Data.EF.UnitTest.Test
{
    public class AEntityDataModel
    {
        protected Dictionary<string, object> MakeDictionary(string source)
        {
            return string.IsNullOrWhiteSpace(source)
                ? new Dictionary<string, object>()
                : JsonConvert.DeserializeObject<Dictionary<string, object>>(source);
        }
    }
}