using System.Collections.Generic;
using Newsdeck.Data.Data;
using Newtonsoft.Json;

namespace Newsdeck.Data.EF.UnitTest.Test.Blob
{
    public abstract class ABlobDataModel : AEntityDataModel, IBlobData
    {
        public Dictionary<string, object> Data { get; protected set; } = new Dictionary<string, object>();
        public Dictionary<string, object> HandlerData { get; protected set; } = new Dictionary<string, object>();

        public string OriginalUrl { get; set; }
        public string ContentType { get; set; }

        public string JsonData => JsonConvert.SerializeObject(Data);

        public string HandlerJsonData => JsonConvert.SerializeObject(HandlerData);
    }
}