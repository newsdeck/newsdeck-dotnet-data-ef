using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;
using Newsdeck.Data.EF.Repository;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF
{
    internal class EFWriteSession : EFReadSession, IWriteSession
    {

        private readonly IDbContextTransaction _transaction;

        /// <summary>
        /// Store the write session completed result state
        ///   null: not completed
        ///   true: has commit
        ///   false: has rollback
        /// </summary>
        private bool? _completedResult;
        
        public EFWriteSession(NewsdeckDbContext dbContext) 
            : base(dbContext)
        {
            _transaction = dbContext.Database.BeginTransaction();
        }

        public IBlobWriteRepository GetBlobWriteRepository()
        {
            return new EFBlobWriteRepository(this);
        }

        public IPublicationWriteRepository GetPublicationWriteRepository()
        {
            return new EFPublicationWriteRepository(this);
        }

        public ITagWriteRepository GetTagWriteRepository()
        {
            return new EFTagWriteRepository(this);
        }

        public ISourceWriteRepository GetSourceWriteRepository()
        {
            return new EFSourceWriteRepository(this);
        }

        public async Task CommitAsync(CancellationToken cancellationToken)
        {
            if (_completedResult == null)
            {
                await _transaction.CommitAsync(cancellationToken);
                _completedResult = true;
            }
        }

        public async Task RollbackAsync(CancellationToken cancellationToken)
        {
            if (_completedResult == null)
            {
                await _transaction.RollbackAsync(cancellationToken);
                _completedResult = false;
            }
        }

        public new async ValueTask DisposeAsync()
        {
            await RollbackAsync(CancellationToken.None);
            await base.DisposeAsync();
        }
    }
}