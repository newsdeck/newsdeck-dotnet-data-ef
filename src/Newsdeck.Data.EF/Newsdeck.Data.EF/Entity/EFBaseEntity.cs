using System.ComponentModel.DataAnnotations.Schema;

namespace Newsdeck.Data.EF.Entity
{
    internal abstract class EFBaseEntity
    {
        [Column("creationDate")]
        public long CreationUnixTime { get; set; }
        
        [Column("lastModificationDate")]
        public long LastModificationUnixTime { get; set; }
    }
}