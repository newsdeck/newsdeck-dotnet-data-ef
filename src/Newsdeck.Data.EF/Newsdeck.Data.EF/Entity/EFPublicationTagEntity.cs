using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace Newsdeck.Data.EF.Entity
{
    [Table("publicationTag")]
    internal class EFPublicationTagEntity
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Identifier { get; set; }
        
        [Column("tagId")]
        public int TagIdentifier { get; set; }
        [Column("publicationId")]
        public long PublicationIdentifier { get; set; }
        
        public virtual EFTagEntity Tag { get; set; }
        public virtual EFPublicationEntity Publication { get; set; }

    }
}