using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Data;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Model;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF.Repository
{
    internal class EFBlobWriteRepository : EFBlobReadRepository, IBlobWriteRepository
    {
        public new IWriteSession Session => _efWriteSession;

        private readonly EFWriteSession _efWriteSession;
        
        
        public EFBlobWriteRepository(EFWriteSession efReadSession) : base(efReadSession)
        {
            _efWriteSession = efReadSession;
        }

        public async Task<long> CreateAsync(ICreateBlobModel model, CancellationToken cancellationToken)
        {
            var set = GetDebSet();
            var res = await set.AddAsync(ToEntity(model), cancellationToken);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
            return res.Entity.Identifier;
        }

        public async Task EditAsync(long identifier, IEditBlobModel model, CancellationToken cancellationToken)
        {
            var item = await _GetWithIdAsync(identifier, cancellationToken);
            Apply(item, model);
            GetDebSet().Update(item);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(long identifier, CancellationToken cancellationToken)
        {
            var item = await _GetWithIdAsync(identifier, cancellationToken);
            GetDebSet().Remove(item);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        private EFBlobEntity ToEntity(ICreateBlobModel model)
        {
            var b = model.ToEntity<EFBlobEntity>();
            _ApplyData(b, model);
            return b;
        }

        private void Apply(EFBlobEntity entity, IEditBlobModel model)
        {
            model.ApplyTo(entity);
            _ApplyData(entity, model);
        }

        private void _ApplyData(EFBlobEntity entity, IBlobData data)
        {
            entity.JsonData = data.JsonData;
            entity.ContentType = data.ContentType;
            entity.OriginalUrl = data.OriginalUrl;
            entity.HandlerJsonData = data.HandlerJsonData;
        }
    }
}