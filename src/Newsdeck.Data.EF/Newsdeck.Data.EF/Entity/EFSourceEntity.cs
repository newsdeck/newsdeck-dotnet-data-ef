using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using Newsdeck.Data.Entity;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace Newsdeck.Data.EF.Entity
{
    [Table("source")]
    internal class EFSourceEntity : EFBaseEntity, ISourceEntity
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Identifier { get; set; }
        
        [Column("type")]
        public string Type { get; set; }
        
        [Column("jsonData")]
        public string JsonData { get; set; }

        [Column("nextCheckAfterUnixTime")]
        public long NextCheckAfterUnixTime { get; set; }
        
        [Column("lastTimeChangedUnixTime")]
        public long LastTimeChangedUnixTime { get; set; }
        
        [Column("sourceIconId")]
        public long? SourceIconIdentifier { get; set; }
        public virtual ICollection<EFPublicationEntity> PublicationsEF { get; set; }
        public virtual ICollection<EFSourceUrlEntity> SourceUrlEF { get; set; }
        public virtual EFBlobEntity SourceIconEF { get; set; }

        [NotMapped]
        public IEnumerable<IPublicationEntity> Publications => PublicationsEF;
        
        [NotMapped]
        public IBlobEntity SourceIcon => SourceIconEF;
    }
}