using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using Newsdeck.Data.Entity;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace Newsdeck.Data.EF.Entity
{
    [Table("publication")]
    internal class EFPublicationEntity : EFBaseEntity, IPublicationEntity
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Identifier { get; set; }
        
        [Column("jsonData")]
        public string JsonData { get; set; }
        
        [Column("sourceId")]
        public long SourceIdentifier { get; set; }
        
        [Column("publicationCoverId")]
        public long? PublicationCoverIdentifier { get; set; }
        
        [Column("originalGuid")]

        public string OriginalGuid { get; set; }

        [Column("publicationDate")]
        public long PublicationDateUnixTime { get; set; }

        public virtual EFSourceEntity SourceEF { get; set; }
        public virtual EFBlobEntity PublicationCoverEF { get; set; }
        public virtual ICollection<EFPublicationTagEntity> TagsEF { get; set; }

        [NotMapped]
        public IBlobEntity PublicationCover => PublicationCoverEF;

        [NotMapped]
        public ISourceEntity Source => SourceEF;

        [NotMapped]
        public IEnumerable<ITagEntity> Tags => TagsEF.Select(o => o.Tag);
    }
}