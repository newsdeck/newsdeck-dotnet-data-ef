using System.Threading.Tasks;
using Newsdeck.Data.EF.Repository;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF
{
    internal class EFReadSession : IReadSession
    {
        public NewsdeckDbContext Context => _dbContext;
        
        private readonly NewsdeckDbContext _dbContext;

        public EFReadSession(NewsdeckDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ValueTask DisposeAsync()
        {
            return _dbContext.DisposeAsync();
        }

        public IBlobReadRepository GetBlobReadRepository()
        {
            return new EFBlobReadRepository(this);
        }

        public IPublicationReadRepository GetPublicationReadRepository()
        {
            return new EFPublicationReadRepository(this);
        }

        public ITagReadRepository GetTagReadRepository()
        {
            return new EFTagReadRepository(this);
        }

        public ISourceReadRepository GetSourceReadRepository()
        {
            return new EFSourceReadRepository(this);
        }
    }
}