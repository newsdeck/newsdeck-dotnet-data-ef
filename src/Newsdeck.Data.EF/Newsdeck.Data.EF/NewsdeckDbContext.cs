using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newsdeck.Data.EF.Entity;

namespace Newsdeck.Data.EF
{
    internal class NewsdeckDbContext : DbContext
    {
        
        
        public DbSet<EFBlobEntity> Blobs { get; set; }
        
        public DbSet<EFPublicationEntity> Publications { get; set; }
        
        public DbSet<EFTagEntity> Tags { get; set; }
        
        public DbSet<EFSourceEntity> Sources { get; set; }
        
        public DbSet<EFSourceUrlEntity> SourceUrl { get; set; }
        
        public DbSet<EFPublicationTagEntity> PublicationsTags { get; set; }

        
        private readonly EFNewsdeckConfiguration _configuration;
        private readonly ILoggerFactory _loggerFactory;

        public NewsdeckDbContext(EFNewsdeckConfiguration configuration,
            ILoggerFactory loggerFactory)
        {
            _configuration = configuration;
            _loggerFactory = loggerFactory;
            _configuration.OnContextInitialization?.Invoke(this);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            _configuration.DbContextOptionBuilder.Invoke(options);
            options.UseLazyLoadingProxies();
            options.UseLoggerFactory(_loggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EFPublicationEntity>()
                .HasOne(p => p.SourceEF)
                .WithMany(b => b.PublicationsEF)
                .HasForeignKey(o => o.SourceIdentifier);
            
            modelBuilder.Entity<EFSourceEntity>()
                .HasMany(p => p.SourceUrlEF)
                .WithOne(b => b.SourceEF)
                .HasForeignKey(o => o.SourceIdentifier);
            
            modelBuilder.Entity<EFPublicationEntity>()
                .HasOne(p => p.PublicationCoverEF)
                .WithMany()
                .HasForeignKey(o => o.PublicationCoverIdentifier);
            
            modelBuilder.Entity<EFSourceEntity>()
                .HasOne(p => p.SourceIconEF)
                .WithMany()
                .HasForeignKey(o => o.SourceIconIdentifier);

            modelBuilder.Entity<EFSourceUrlEntity>()
                .HasOne(p => p.SourceEF)
                .WithMany(o => o.SourceUrlEF)
                .HasForeignKey(o => o.SourceIdentifier);

            modelBuilder.Entity<EFPublicationTagEntity>()
                .HasOne(p => p.Publication)
                .WithMany(o => o.TagsEF)
                .HasForeignKey(o => o.PublicationIdentifier);

            modelBuilder.Entity<EFPublicationTagEntity>()
                .HasOne(p => p.Tag)
                .WithMany()
                .HasForeignKey(o => o.TagIdentifier);
        }
    }
}