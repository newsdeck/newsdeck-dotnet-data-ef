using System;
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Data;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Model;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF.Repository
{
    internal class EFSourceWriteRepository : EFSourceReadRepository, ISourceWriteRepository
    {

        private readonly EFWriteSession _efWriteSession;
        
        public EFSourceWriteRepository(EFWriteSession efWriteSession)
            : base(efWriteSession)
        {
            _efWriteSession = efWriteSession;
        }

        public new IWriteSession Session => _efWriteSession;
        
        public async Task<long> CreateAsync(ICreateSourceModel model, CancellationToken cancellationToken)
        {
            var set = GetDebSet();
            var res = await set.AddAsync(ToEntity(model), cancellationToken);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
            return res.Entity.Identifier;
        }

        public async Task EditAsync(long identifier, IEditSourceModel model, CancellationToken cancellationToken)
        {
            var item = await _GetWithIdAsync(identifier, cancellationToken);
            Apply(item, model);
            GetDebSet().Update(item);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task AddUrlToSource(long sourceIdentifier, string url, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                throw new ArgumentNullException(nameof(url));
            }
            var finalUrl = url.Trim().ToLowerInvariant();
            var item = await _GetWithIdAsync(sourceIdentifier, cancellationToken);
            await EFReadSession.Context.SourceUrl.AddAsync(new EFSourceUrlEntity
            {
                Url = finalUrl,
                SourceIdentifier = item.Identifier
            }, cancellationToken);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(long identifier, CancellationToken cancellationToken)
        {
            var item = await _GetWithIdAsync(identifier, cancellationToken);
            GetDebSet().Remove(item);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        private EFSourceEntity ToEntity(ICreateSourceModel model)
        {
            var b = model.ToEntity<EFSourceEntity>();
            _ApplyData(b, model);
            return b;
        }

        private void Apply(EFSourceEntity entity, IEditSourceModel model)
        {
            model.ApplyTo(entity);
            _ApplyData(entity, model);
        }

        private void _ApplyData(EFSourceEntity entity, ISourceData data)
        {
            entity.Type = data.Type;
            entity.JsonData = data.JsonData;
            entity.NextCheckAfterUnixTime = data.NextCheckAfterUnixTime;
            entity.LastTimeChangedUnixTime = data.LastTimeChangedUnixTime;
            entity.SourceIconIdentifier = data.SourceIconIdentifier;
        }
    }
}