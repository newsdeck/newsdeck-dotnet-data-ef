using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF.Repository
{
    internal class EFSourceReadRepository : EFBaseReadRepository<EFSourceEntity, ISourceEntity, long>, ISourceReadRepository
    {
        public EFSourceReadRepository(EFReadSession efReadSession) 
            : base(efReadSession)
        {
        }

        protected override DbSet<EFSourceEntity> GetDebSet()
        {
            return EFReadSession.Context.Sources;
        }

        public async Task<ISourceEntity> FindMatchingSource(string url, CancellationToken cancellationToken)
        {
            var m = url.Trim().ToLowerInvariant();
            var match = await EFReadSession.Context.SourceUrl
                .Where(o => o.Url.Equals(m))
                .SingleOrDefaultAsync(cancellationToken);
            return match?.SourceEF;
        }

        public IDataResult<ISourceEntity, long> Get()
        {
            return new EntityDataResult<ISourceEntity, EFSourceEntity, long>(GetDebSet().AsQueryable());
        }
    }
}