using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Data;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Model;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF.Repository
{
    internal class EFTagWriteRepository : EFTagReadRepository, ITagWriteRepository
    {

        private readonly EFWriteSession _efWriteSession;
        
        public EFTagWriteRepository(EFWriteSession efWriteSession)
            : base(efWriteSession)
        {
            _efWriteSession = efWriteSession;
        }

        public new IWriteSession Session => _efWriteSession;
        
        public async Task<int> CreateAsync(ICreateTagModel model, CancellationToken cancellationToken)
        {
            var set = GetDebSet();
            var res = await set.AddAsync(ToEntity(model), cancellationToken);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
            return res.Entity.Identifier;
        }

        public async Task EditAsync(int identifier, IEditTagModel model, CancellationToken cancellationToken)
        {
            var item = await _GetWithIdAsync(identifier, cancellationToken);
            Apply(item, model);
            GetDebSet().Update(item);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(int identifier, CancellationToken cancellationToken)
        {
            var item = await _GetWithIdAsync(identifier, cancellationToken);
            GetDebSet().Remove(item);
            await _efWriteSession.Context.SaveChangesAsync(cancellationToken);
        }

        private EFTagEntity ToEntity(ICreateTagModel model)
        {
            var b = model.ToEntity<EFTagEntity>();
            _ApplyData(b, model);
            return b;
        }

        private void Apply(EFTagEntity entity, IEditTagModel model)
        {
            model.ApplyTo(entity);
            _ApplyData(entity, model);
        }

        private void _ApplyData(EFTagEntity entity, ITagData data)
        {
            entity.Label = data.Label;
        }
    }
}