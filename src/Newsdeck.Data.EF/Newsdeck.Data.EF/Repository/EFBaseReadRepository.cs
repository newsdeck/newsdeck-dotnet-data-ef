using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Entity;

namespace Newsdeck.Data.EF.Repository
{
    internal abstract class EFBaseReadRepository<TEF, TContract, TIdentifier> 
        where TEF : EFBaseEntity, TContract
        where TContract : IBaseEntity<TIdentifier>
    {
        protected readonly EFReadSession EFReadSession;
        
        public IReadSession Session => EFReadSession;

        public EFBaseReadRepository(EFReadSession efReadSession)
        {
            EFReadSession = efReadSession;
        }
        
        protected async Task<TEF> _GetWithIdAsync(TIdentifier id, CancellationToken cancellationToken)
        {
            return await GetDebSet().FindAsync(new object[]{ id }, cancellationToken);
        }
        
        public async Task<TContract> GetWithIdAsync(TIdentifier id, CancellationToken cancellationToken)
        {
            return await _GetWithIdAsync(id, cancellationToken);
        }

        protected abstract DbSet<TEF> GetDebSet();
    }
}