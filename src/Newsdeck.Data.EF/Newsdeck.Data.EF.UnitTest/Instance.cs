using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newsdeck.Data.EF.UnitTest.Log;

namespace Newsdeck.Data.EF.UnitTest
{
    public class Instance : IAsyncDisposable
    {

        public const string InstanceDefaultDatabaseFilepath = "./db1.sqlite";

        public static Instance Get(string databaseFilepath = InstanceDefaultDatabaseFilepath)
        {
            var instance = new Instance(databaseFilepath);
            instance.InitializeAsync().ConfigureAwait(false);
            return instance;
        }

        private readonly ServiceProvider _serviceProvider;

        private Instance(string databaseFilepath)
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddEFNewsdeckData(conf =>
            {
                if (!databaseFilepath.Equals(InstanceDefaultDatabaseFilepath))
                {
                    conf.OnContextInitialization = context =>
                    {
                        context.Database.EnsureCreated();
                    };
                }
                conf.DbContextOptionBuilder = options =>
                {
                    options.UseSqlite($"Data Source={databaseFilepath}");
                    options.UseLoggerFactory(new LoggerFactory());
                };
            });
            serviceCollection.AddLogging(conf =>
            {
                conf.SetMinimumLevel(LogLevel.Trace);
                conf.AddProvider(new TestLogProvider());
            });
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        public T GetInstance<T>()
        {
            return _serviceProvider.GetService<T>();
        }

        private static bool _wasInit = false;
        public async Task InitializeAsync()
        {
            if (_wasInit)
            {
                return;
            }
            _wasInit = true;
            var logger = _serviceProvider.GetService<ILogger<Instance>>();
            logger?.LogInformation("Init OK.");
        }

        public ValueTask DisposeAsync()
        {
            _serviceProvider?.Dispose();
            return new ValueTask(Task.CompletedTask);
        }
    }
}