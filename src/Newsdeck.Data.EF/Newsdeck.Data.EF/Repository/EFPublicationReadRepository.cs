using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF.Repository
{
    internal class EFPublicationReadRepository : EFBaseReadRepository<EFPublicationEntity, IPublicationEntity, long>, IPublicationReadRepository
    {
        public EFPublicationReadRepository(EFReadSession efReadSession) 
            : base(efReadSession)
        {
        }

        protected override DbSet<EFPublicationEntity> GetDebSet()
        {
            return EFReadSession.Context.Publications;
        }
        
        public new async Task<IPublicationEntity> GetWithIdAsync(long id, CancellationToken cancellationToken)
        {
           return await GetWithInclusion()
                .Where(o => o.Identifier.Equals(id))
                .FirstOrDefaultAsync(cancellationToken);
        }

        private IQueryable<EFPublicationEntity> GetWithInclusion()
        {
            return GetDebSet()
                .AsQueryable()
                .Include(o => o.TagsEF)
                .ThenInclude(o2 => o2.Tag);
        }

        public IDataResult<IPublicationEntity, long> Get()
        {
            return new EntityDataResult<IPublicationEntity, EFPublicationEntity, long>(GetWithInclusion());
        }
    }
}