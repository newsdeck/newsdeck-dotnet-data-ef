using Microsoft.EntityFrameworkCore;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.EF.Repository
{
    internal class EFTagReadRepository : EFBaseReadRepository<EFTagEntity, ITagEntity, int>, ITagReadRepository
    {
        public EFTagReadRepository(EFReadSession efReadSession)
            : base(efReadSession)
        {
        }

        protected override DbSet<EFTagEntity> GetDebSet()
        {
            return EFReadSession.Context.Tags;
        }

        public IDataResult<ITagEntity, int> Get()
        {
            return new EntityDataResult<ITagEntity, EFTagEntity, int>(GetDebSet().AsQueryable());
        }
    }
}