using System;
using Microsoft.Extensions.DependencyInjection;

namespace Newsdeck.Data.EF
{
    /// <summary>
    /// NewsdeckDataExtension specific for Newsdeck.Data.EF
    /// </summary>
    public static class NewsdeckDataExtension
    {
        /// <summary>
        /// Setup Newsdeck.Data.EF into injection service from given IServiceCollection
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configurator"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void AddEFNewsdeckData(this IServiceCollection services, Action<IEFNewsdeckConfiguration> configurator)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            var conf = new EFNewsdeckConfiguration();
            configurator.Invoke(conf);

            if (conf.DbContextOptionBuilder == null)
            {
                throw new ArgumentNullException(nameof(conf.DbContextOptionBuilder));
            }
            
            services.AddSingleton(conf);
            services.AddSingleton<INewsdeckSessionProvider, EFNewsdeckSessionProvider>();
        }
    }
}