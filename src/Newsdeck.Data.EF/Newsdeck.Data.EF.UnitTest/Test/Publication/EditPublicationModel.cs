using Newsdeck.Data.Entity;
using Newsdeck.Data.Model;

namespace Newsdeck.Data.EF.UnitTest.Test.Publication
{
    public class EditPublicationModel : APublicationModel, IEditPublicationModel
    {
        public EditPublicationModel(IPublicationEntity entity)
        {
            Data = MakeDictionary(entity.JsonData);
            SourceIdentifier = entity.SourceIdentifier;
            PublicationCoverIdentifier = entity.PublicationCoverIdentifier;
        }
    }
}