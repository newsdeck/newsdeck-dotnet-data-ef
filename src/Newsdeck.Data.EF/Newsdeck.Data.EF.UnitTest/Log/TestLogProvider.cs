using System;
using Microsoft.Extensions.Logging;

namespace Newsdeck.Data.EF.UnitTest.Log
{
    public class TestLogProvider : ILoggerProvider
    {

        public void Dispose()
        {
            // nothing to do
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new TalendyTestLogger(categoryName, "nUnit-TEST:: ");
        }
        
        private class TalendyTestLogger : ILogger
        {
            private readonly string _logPrefix;
            private readonly string _categoryName;
            
            internal TalendyTestLogger(string categoryName, string logPrefix)
            {
                _logPrefix = logPrefix;
                _categoryName = categoryName;
            }
            
            public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
            {
                string message = _logPrefix;
                if (formatter != null)
                {
                    message += formatter(state, exception);
                }
                // Implement log writter as you want. I am using Console
                Console.WriteLine($"{DateTime.Now:O} | {logLevel.ToString()} - {eventId.Id} - {_categoryName} - {message}");
            }

            public bool IsEnabled(LogLevel logLevel)
            {
                return true;
            }

            public IDisposable BeginScope<TState>(TState state)
            {
                // nothing to do
                return new NoopDisposable();
            }
            
            private class NoopDisposable : IDisposable
            {
                public void Dispose()
                {
                }
            }
        }
    }
}