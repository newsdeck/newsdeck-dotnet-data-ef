using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using Newsdeck.Data.Entity;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace Newsdeck.Data.EF.Entity
{
    [Table("tag")]
    internal class EFTagEntity : EFBaseEntity, ITagEntity
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Identifier { get; set; }

        [Column("label")]
        public string Label { get; set; }
    }
}