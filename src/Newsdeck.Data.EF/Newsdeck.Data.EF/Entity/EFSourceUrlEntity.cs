using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace Newsdeck.Data.EF.Entity
{
    [Table("sourceUrl")]
    internal class EFSourceUrlEntity
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Identifier { get; set; }
        
        [Column("sourceId")]
        public long SourceIdentifier { get; set; }
        
        [Column("url")]
        public string Url { get; set; }
        
        public virtual EFSourceEntity SourceEF { get; set; }
    }
}