using Newsdeck.Data.Model;

namespace Newsdeck.Data.EF.UnitTest.Test.Source
{
    public class TestSourceModel : IEditSourceModel, ICreateSourceModel
    {
        public string Type { get; set; }
        public string JsonData { get; set; }
        public long NextCheckAfterUnixTime { get; set; }
        public long LastTimeChangedUnixTime { get; set; }
        public long? SourceIconIdentifier { get; set; }
    }
}