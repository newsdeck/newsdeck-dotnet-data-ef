using System;
using Microsoft.EntityFrameworkCore;

namespace Newsdeck.Data.EF
{
    /// <summary>
    /// Configuration exposition for DI 
    /// </summary>
    public interface IEFNewsdeckConfiguration
    {
        /// <summary>
        /// Default configuration for EF DbContext
        /// </summary>
        Action<DbContextOptionsBuilder> DbContextOptionBuilder { get; set; }
        
        /// <summary>
        /// Optional call on DbContext (NewsdeckDbContext) constructor
        /// </summary>
        Action<DbContext> OnContextInitialization { get; set; }
    }
}