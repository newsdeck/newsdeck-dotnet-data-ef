using System;
using Newsdeck.Data.EF.Entity;
using Newsdeck.Data.Model;

namespace Newsdeck.Data.EF.Repository
{
    internal static class EFWriteRepositoryExtension
    {
        public static T ToEntity<T>(this ICreateBaseModel model) 
            where T : EFBaseEntity, new()
        {
            return new T
            {
                CreationUnixTime = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                LastModificationUnixTime = DateTimeOffset.Now.ToUnixTimeMilliseconds()
            };
        }

        public static void ApplyTo<T>(this IEditBaseModel model, T entity) 
            where T : EFBaseEntity
        {
            entity.LastModificationUnixTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
        }
    }
}