using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newsdeck.Data.DataException;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Filter;
using Newsdeck.Data.Order;

namespace Newsdeck.Data.EF
{
    internal class EntityDataResult<TContract, TEntity, TIdentifier> : IDataResult<TContract, TIdentifier>,
        IFilterEvaluator<IQueryable<TEntity>, TContract>
        where TEntity : class, TContract
        where TContract : IBaseEntity<TIdentifier>
    {
        private IQueryable<TEntity> _source;
        private readonly Dictionary<Type, Delegate> _dictionary;

        public EntityDataResult(IQueryable<TEntity> source)
        {
            _dictionary = new Dictionary<Type, Delegate>();
            this.RegisterBase();
            _source = source.AsQueryable();
        }

        private Delegate LoadEvaluator(object f)
        {
            var type = f.GetType();
            var evaluator = _dictionary[type];
            if (evaluator == null)
            {
                throw new EvaluatorMissingException(type);
            }
            return evaluator;
        }
        
        private IQueryable<TEntity> Evaluate(IFilter f)
        {
            var evaluator = LoadEvaluator(f);
            // we made sure that the parameters are correct when adding in the dic
            var r = evaluator.DynamicInvoke(f);
            return r == null ? null : (r is IQueryable<TEntity> s ? s : null);
        }
        
        private IQueryable<TEntity> Evaluate(IOrder o)
        {
            var evaluator = LoadEvaluator(o);
            // we made sure that the parameters are correct when adding in the dic
            var r = evaluator.DynamicInvoke(o);
            return r == null ? null : (r is IQueryable<TEntity> s ? s : null);
        }

        public async Task<List<TContract>> AsListAsync(CancellationToken cancellationToken)
        {
            var r = await _source.ToListAsync(cancellationToken);
            return new List<TContract>(r);
        }

        public void AddFilter(IFilter filter)
        {
            _source = Evaluate(filter);
        }

        public void AddOrder(IOrder order)
        {
            _source = Evaluate(order);
        }

        public void SetLimit(int? limit)
        {
            _source = limit.HasValue ? _source.Take(limit.Value) : _source;
        }

        private Expression OfVal(object val)
        {
            return Expression.Constant(val);
        }

        public void RegisterFilterEvaluator<TFilter>(Func<TFilter, IQueryable<TEntity>> r) 
            where TFilter : IFilter
        {
            _dictionary[typeof(TFilter)] = r;
        }

        public void RegisterOrderEvaluator<TOrder>(Func<TOrder, IQueryable<TEntity>> r) 
            where TOrder : IOrder
        {
            _dictionary[typeof(TOrder)] = r;
        }

        public IQueryable<TEntity> EvaluateAnd(AndFilter f)
        {
            foreach (var fFilter in f.Filters)
            {
                _source = Evaluate(fFilter);
            }
            return _source;
        }

        private IQueryable<TEntity> _EvaluateSimpleFilter(PropertyFilter<TContract> f, 
            Func<MemberExpression, BinaryExpression> binaryExpression)
        {
            _source = _source.QWhere(f, binaryExpression);
            return _source;
        }

        private IQueryable<TEntity> _EvaluateSimpleOrder(AOrder<TContract> f, 
            bool asc)
        {
            _source = _source.QOrder(f, asc);
            return _source;
        }
        
        public IQueryable<TEntity> EvaluateEqualFilter(EqualFilter<TContract> f)
        {
            return _EvaluateSimpleFilter(f, 
                o => Expression.Equal(o, OfVal(f.Value)));
        }

        public IQueryable<TEntity> EvaluateNotEqualFilter(DifferentFilter<TContract> f)
        {
            return _EvaluateSimpleFilter(f, 
                o => Expression.NotEqual(o, OfVal(f.Value)));
        }

        public IQueryable<TEntity> EvaluateIn(InFilter<TContract> f)
        {
            return _EvaluateSimpleFilter(f, o =>
                {
                    var expressions = f.Values.Select(OfVal);
                    BinaryExpression res = null;
                    foreach (var constantExpression in expressions)
                    {
                        var expr = Expression.Equal(o, constantExpression);
                        res = res == null
                            ? expr
                            : Expression.Or(res, expr);
                    }
                    return res;
                });
        }

        public IQueryable<TEntity> EvaluateLowerOrEqualThenFilter(LowerOrEqualThenFilter<TContract> f)
        {
            
            return _EvaluateSimpleFilter(f, 
                o => Expression.GreaterThanOrEqual(OfVal(f.Value), o));
        }

        public IQueryable<TEntity> EvaluateLowerThenFilter(LowerThenFilter<TContract> f)
        {
            return _EvaluateSimpleFilter(f, 
                o => Expression.GreaterThan(OfVal(f.Value), o));
        }

        public IQueryable<TEntity> EvaluateGreaterOrEqualThenFilter(GreaterOrEqualThenFilter<TContract> f)
        {
            return _EvaluateSimpleFilter(f, 
                o => Expression.GreaterThanOrEqual(o, OfVal(f.Value)));
        }

        public IQueryable<TEntity> EvaluateGreaterThenFilter(GreaterThenFilter<TContract> f)
        {
            return _EvaluateSimpleFilter(f, 
                o => Expression.GreaterThan(o, OfVal(f.Value)));
        }

        public IQueryable<TEntity> EvaluateDescOrder(DescOrder<TContract> order)
        {
            return _EvaluateSimpleOrder(order, false);
        }

        public IQueryable<TEntity> EvaluateAscOrder(AscOrder<TContract> order)
        {
            return _EvaluateSimpleOrder(order, true);
        }
    }
}